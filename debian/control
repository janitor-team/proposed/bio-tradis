Source: bio-tradis
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: perl
# Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: libbio-perl-perl,
                     libenv-path-perl,
                     libexception-class-perl,
                     libmoose-perl,
                     libtest-exception-perl,
                     libtest-files-perl,
                     libtest-most-perl,
                     libtest-simple-perl,
                     libtext-csv-perl,
                     libtry-tiny-perl,
                     perl,
                     samtools,
                     smalt,
                     tabix,
                     bwa
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/med-team/bio-tradis
Vcs-Git: https://salsa.debian.org/med-team/bio-tradis.git
Homepage: https://github.com/sanger-pathogens/Bio-Tradis

Package: bio-tradis
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libbio-perl-perl,
         libexception-class-perl,
         libmoose-perl,
         libtext-csv-perl,
         libtry-tiny-perl,
         smalt,
         samtools,
         tabix,
         r-base-core,
         r-cran-getopt,
         r-cran-mass
Suggests: artemis,
          r-bioc-edger
Description: analyse the output from TraDIS analyses of genomic sequences
 Bio-Tradis contains a set of tools to analyse the output from
 TraDIS analyses.
 .
 The Bio-Tradis analysis pipeline is implemented as an extensible Perl
 library which can either be used as is, or as a basis for the
 development of more advanced analysis tools.
 .
 Please note: You need to manually install BioConductor Edger which can
 not be distributed by Debian in recent version since it is using
 non-distributable code locfit.
